package edu.upenn.cis.cis455.m1.server;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
	final static Logger logger = LogManager.getLogger(HttpTaskQueue.class);
	private int size = 5; //Same as the thread pool size, default is 3
	private List<HttpTask> queue;
	
	//Constructor
	public HttpTaskQueue() {
		queue = new ArrayList<HttpTask>(size);
	}
	public HttpTaskQueue(int size) {
		this.size = size;
		queue = new ArrayList<HttpTask>(size);
		
	}
	
	/**
	 * Put the new object to the end of queue
	 * @throws InterruptedException
	 */
	public void put(HttpTask task) throws InterruptedException {	
	  	synchronized(queue){
			queue.add(task);
			queue.notify();
		}
	}

	/**
	 * Take the first object from the queue
	 * @throws InterruptedException
	 */
	
	public HttpTask take() throws InterruptedException {
		while(queue.isEmpty()){
			synchronized(queue) {
				queue.wait();
			}
		}
		synchronized(queue){
			return queue.remove(0);
		}
	}
	
	/**	
	 * Check if the queue is empty
	 * @return boolean
	 */
	public boolean isEmpty() {
		if (queue.isEmpty()) {
			return true;
		}
		return false;
	}
}
