/**
 * CIS 455/555 route-based HTTP framework
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import edu.upenn.cis.cis455.exceptions.HaltException;

public class WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);

    /**
     * Params
     */
    protected HttpListener listener;
    public static String directory;
    private String ipAddress = "0.0.0.0";
    protected int port;
    public static int threads = 3;
    public static List<Thread> pool;
    protected HttpTaskQueue taskQueue = new HttpTaskQueue(50);
    private HttpWorker[] workers;
    public static Map<String, String> threadStatus = new HashMap<String, String>();
    private Thread listenerThread;
    
	public WebService() { 

	}
	
	/**
	 * Construct WebService with exist taskQueue
	 * @param taskQueue
	 */
//	public WebService(HttpTaskQueue taskQueue) { 
//		this.taskQueue = taskQueue;
//		try {
//			listener = new HttpListener(port, directory, taskQueue);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	/**
	 * Construct WebService with exist taskQueue,root directory, and port
	 * @param taskQueue
	 */
//	public WebService(int portIn, String dir, HttpTaskQueue taskQueue) { 
//		this.taskQueue = taskQueue;
//		directory = dir;
//		port = portIn;
//		try {
//			listener = new HttpListener(port, directory, taskQueue);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

    /**
     * Launches the Web server thread pool and the listener
     * @throws IOException 
     */
    public void start() {
    	listener = new HttpListener(port, directory, taskQueue);
    	//Start a listener thread
    	listenerThread = new Thread(listener);
    	listenerThread.start();
    	//Create worker thread pool
		pool = new ArrayList<Thread>(threads);
		workers = new HttpWorker[threads];
		//Put worker threads in and record the status
		for (int i = 0; i < threads; i++){
			HttpWorker worker = new HttpWorker(taskQueue, i);
			workers[i] = worker;
			pool.add(new Thread(worker));
			String threadName = pool.get(i).getName();
			threadStatus.put(threadName,"Waiting");
			workers[i].setName(threadName);
			pool.get(i).start();
		}
    }

    /**
     * Gracefully shut down the server
     */
    public void stop() {
    	//Shut down the listener
    	listener.interrupt();
    	//Shut down the listener thread
    	listenerThread.interrupt();
    	//Wait until taskQueue is empty
    	while (!taskQueue.isEmpty()) {
    	}
    	//Shut down the workers and worker threads
		for (int i = 0; i < threads; i++){
			threadStatus.put(pool.get(i).getName(), "shutdown");
			workers[i].shutdown();
			pool.get(i).interrupt();
		}
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     * @throws IOException 
     */
    public void awaitInitialization() throws IOException {
        logger.info("Initializing server");
        start();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
    	this.directory = directory;
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {
    	this.ipAddress = ipAddress;
    }

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {
    	this.port = port;
    }

    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {
    	this.threads= threads;
    }

}
