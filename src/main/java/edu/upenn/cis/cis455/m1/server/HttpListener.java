package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.net.httpserver.HttpServer;

/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {
	final static Logger logger = LogManager.getLogger(HttpListener.class);
	private int port;
	private String directory;
	private ServerSocket serverSocket;
	private final int serverSocketSize = 300;
	private boolean acceptRequest = true;
	private HttpTaskQueue taskQueue;
	
	public HttpListener(int port, String directory, HttpTaskQueue taskQueue){
		this.port = port;
		this.directory = directory;
		this.taskQueue = taskQueue;
	}
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String dir) {
		directory = dir;
	}
	
	
	/**
	 * Keep accept request until interrupted
	 */
	public void run() {	
		try {
			serverSocket = new ServerSocket(port, serverSocketSize);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//while accepting requests
		while (acceptRequest){
			try{
				Socket socket = serverSocket.accept();
				taskQueue.put(new HttpTask(socket));
			}catch (IOException e) {
				logger.info("Server socket closed");
			} catch (InterruptedException e) {
				logger.error("Could not add/receive task");
				e.printStackTrace();
			} 

		}
    }
	
	/**
	 * Interrupt the listener Object
	 */
	public void interrupt(){
		//stop accepting requests		
		this.acceptRequest = false;
		try {
			serverSocket.close();
		} catch (IOException e) {
			logger.error("Can not close serverSocket");
		}
	}	
}
