package edu.upenn.cis.cis455.m1.handling;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;

/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
    
    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     * @throws IOException 
     */
    public static boolean sendException(Socket socket, Request request, Response response, HaltException except) throws IOException {
    	DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
    	//Write exception info to socket
    	int errorStatus = except.statusCode();
    	response.status(errorStatus);
        dos.writeBytes(response.getHeaders());
//    	dos.writeBytes(Integer.toString(errorStatus)+ " " + getErrorInfo(errorStatus));
        dos.flush(); // send the message
        dos.close(); // close the output stream when we're done.
        logger.info("Exception sent: " + errorStatus);
        socket.close();
        return true;
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     * @throws IOException 
     */
    public static boolean sendResponse(Socket socket, Request request, Response response) throws IOException {
    	//Write response info to socket
    	DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        dos.writeBytes(response.getHeaders());
        //Check if it is a HEAD response or not
        if (response.bodyRaw().length!=0) {
        	dos.write(response.bodyRaw());
        }
        dos.flush(); // send the message
        dos.close(); // close the output stream when we're done.
        socket.close();
        logger.info("Response sent: "+ response.status());
        return true;
    }
    
    /**
     * Get request headers and uri from HttpParsing
     * @param socket
     * @param request
     * @return uri
     * @throws HaltException
     * @throws IOException
     */
    public static String parseRequest(Socket socket, RequestEntity request) throws HaltException, IOException {
    	InputStream inputStream = socket.getInputStream();
//    	String remoteIp = socket.getInetAddress().toString();
    	Map<String, String> headers = new HashMap<String, String>();
    	Map<String, List<String>> parms = new HashMap<String, List<String>>();
    	StringBuffer bufferBody = new StringBuffer();
    	//Get uri
    	String uri = HttpParser.parseRequest("45555", inputStream, headers, parms, bufferBody);
    	//Set headers for the request entity
    	request.setHeaders(headers);
    	request.setQueryParams(parms);
    	request.body(bufferBody.toString());
    	System.out.println(request.body());
    	logger.info("Request parsed, uri: "+uri);
    	return uri;
    }
    
    /**
     * Error code info
     * @param status
     * @return
     */
	public static String getErrorInfo(int status) {
		  if (status == 500) return "Internal Server Error";
		  else if (status == 304) return "Not Modified";
		  else if (status == 400) return "Bad Request";
		  else if (status == 401) return "Unauthorized";
		  else if (status == 403) return "Forbidden";
		  else if (status == 404) return "Not Found";
		  else if (status == 405) return "Method Not Allowed";
		  else return "unknown";
		 }
    
}
