package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;

import java.net.Socket;
import java.net.SocketException;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.RequestEntity;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseEntity;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Stub class for a thread worker that handles Web requeststs
 */
public class HttpWorker implements Runnable {
    final static Logger logger = LogManager.getLogger(HttpWorker.class);
    
	private HttpTaskQueue taskQueue;
	private HttpTask task;
	private boolean run;
	private String name;

	/**
	 * Construct with exist taskQueue
	 * @param taskQueue
	 * @param num
	 */
	public HttpWorker(HttpTaskQueue taskQueue, int num){
		this.taskQueue = taskQueue;
	}
	
	/**
	 * Set name for the worker's thread
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Keep take Task from the queue
	 */
    @Override
    public void run() {
        // TODO Auto-generated method stub
		run = true;
		while (run){
			Socket socket;
			try {
				socket = taskQueue.take().getSocket();
				if (!socket.isClosed()){	
					handle(socket);		
					logger.info("Socket handled by: "+ this.name);
				}	
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				logger.info(this.name+": Worker shutdown success");
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
    }
    
    /**
     * Handle the got socket
     * @param s
     * @throws Exception 
     */
    public void handle(Socket s) throws Exception {
        RequestEntity request = new RequestEntity();
        Response response = new ResponseEntity();   
        //Get the uri
        String uri = HttpIoHandler.parseRequest(s,request);
        WebService.threadStatus.put(this.name , uri);
        try {
        	//If the uri exist
	    	RequestHandler.generateResponse(uri, s, request, response);
    		HttpIoHandler.sendResponse(s, request, response);
    		WebService.threadStatus.put(this.name , "Waiting");

        }catch (HaltException halt) {    
        	//If the uri not exist
        	HttpIoHandler.sendException(s, request, response, halt);
        	WebService.threadStatus.put(this.name , "Waiting");
        }
    	
    } 

    /**
     * Stop run the thread
     */
    public void shutdown() {
    	this.run=false;
    }
    
}
