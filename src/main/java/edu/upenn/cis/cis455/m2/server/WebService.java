/**
 * CIS 455/555 route-based HTTP framework
 * 
 * Z. Ives, 8/2017
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m2.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m1.server.HttpListener;
import edu.upenn.cis.cis455.m1.server.HttpTaskQueue;
import edu.upenn.cis.cis455.m2.interfaces.Filter;

public class WebService extends edu.upenn.cis.cis455.m1.server.WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);
    public static ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<String, Session>();
    public static ConcurrentHashMap<String, List<Filter>> beforeMap = new ConcurrentHashMap<String, List<Filter>>();
    public static ConcurrentHashMap<String, List<Filter>> afterMap = new ConcurrentHashMap<String,  List<Filter>>();
    public static ConcurrentHashMap<String, Route> getMap = new ConcurrentHashMap<String, Route>();
    public static ConcurrentHashMap<String, Route> postMap = new ConcurrentHashMap<String, Route>();
    public static ConcurrentHashMap<String, Route> putMap = new ConcurrentHashMap<String, Route>();
    public static ConcurrentHashMap<String, Route> deleteMap = new ConcurrentHashMap<String, Route>();
    public static ConcurrentHashMap<String, Route> headMap = new ConcurrentHashMap<String, Route>();
    public static ConcurrentHashMap<String, Route> optionMap = new ConcurrentHashMap<String, Route>();

    public WebService() {
        super();
    }

    ///////////////////////////////////////////////////
    // For more advanced capabilities
    ///////////////////////////////////////////////////
    /**
     * Handle an HTTP POST request to the path
     */
    public void get(String path, Route route) {
    	getMap.put(path, route);
    }
    
    /**
     * Handle an HTTP POST request to the path
     */
    public void post(String path, Route route) {
    	postMap.put(path, route);
    }

    /**
     * Handle an HTTP PUT request to the path
     */
    public void put(String path, Route route) {
    	putMap.put(path,route);
    }

    /**
     * Handle an HTTP DELETE request to the path
     */
    public void delete(String path, Route route) {
    	deleteMap.put(path,route);
    }

    /**
     * Handle an HTTP HEAD request to the path
     */
    public void head(String path, Route route) {
    	headMap.put(path,route);
    }

    /**
     * Handle an HTTP OPTIONS request to the path
     */
    public void options(String path, Route route) {
    	 optionMap.put(path,route);
    }

    ///////////////////////////////////////////////////
    // HTTP request filtering
    ///////////////////////////////////////////////////

    /**
     * Add filters that get called before a request
     */
    public void before(Filter filter) {
    	if (!beforeMap.containsKey("/*")){
    		beforeMap.put("/*", new ArrayList<Filter>());
    	} 
    	beforeMap.get("/*").add(filter);
    }

    /**
     * Add filters that get called after a request
     */
    public void after(Filter filter) {
    	if (!afterMap.containsKey("/*")){
    		afterMap.put("/*", new ArrayList<Filter>());
    	} 
    	afterMap.get("/*").add(filter);
    }

    /**
     * Add filters that get called before a request
     */
    public void before(String path, Filter filter) {
    	if (!beforeMap.containsKey(path)){
    		beforeMap.put(path, new ArrayList<Filter>());
    	} 
    	beforeMap.get(path).add(filter);
    }

    /**
     * Add filters that get called after a request
     */
    public void after(String path, Filter filter) {
    	if (!afterMap.containsKey(path)){
    		afterMap.put(path, new ArrayList<Filter>());
    	} 
    	afterMap.get(path).add(filter);
    }

}
