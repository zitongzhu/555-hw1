package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;
import java.util.Map;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.cis455.m2.interfaces.Session;

public class WebServer {
    public static void main(String[] args) {   

        staticFileLocation("/vagrant/555-hw1/www");          
        port(45555);

        get("/testRoute", (request, response) -> {
        	response.redirect("/:name/hello/", 301);
            return "testRoute content";
        });
            
        get("/:name/hello/", (request, response) -> {
        	response.redirect("/testCookie1", 301);
            String body = "<HTML><BODY><h3>Name Test - </h3>" + request.params("name");
            body += "</BODY></HTML>";
            response.type("text/html");
            response.body(body);
            return "Hello: " + request.params("name");
        });

        get("/testCookie1", (request, response) -> {
          String body = "<HTML><BODY><h3>Cookie Test 1</h3>";
          
          response.cookie("TestCookie1", "1");

          body += "Added cookie (TestCookie,1) to response.";
          response.type("text/html");
          response.body(body);
          return response.body();
        });

        get("/session/get", (request, response) -> {
            String body = "<HTML><BODY><h3>Session Test 1</h3>";
            body += "</BODY></HTML>";
            createSession();
            response.status(200);
            response.body(body);
               response.body(response.getHeaders()+System.lineSeparator()+body);
    
            return "session created";
           });
           
           get("/session/check", (request, response) -> {
            
            Session session = request.session(false);
            if (session == null) return "No built session for the client";
            String body = "<HTML><BODY><h3>Session exits</h3>";
            body += "</BODY></HTML>";
               response.body(body);
               response.body(response.getHeaders()+body);
            return "Hello, client ip: " + session.attribute("ip") + " session-id: " + session.id();
           });
           
        get("/testSession1", (request, response) -> {
          String body = "<HTML><BODY><h3>Session Test 1</h3>";

//          request.session(true).attribute("Attribute1", "Value1");

          body += "</BODY></HTML>";
          response.type("text/html");
          response.body(body);
          return response.body();
        });

        before((request, response) -> {
          request.attribute("attribute1", "everyone");
        });
        get("/testFilter1", (request, response) -> {
          String body = "<HTML><BODY><h3>Filters Test</h3>";

          for(String attribute : request.attributes()) {
            body += "Attribute: " + attribute + " = " + request.attribute(attribute) + "\n";
          }

          body += "</BODY></HTML>";
          response.type("text/html");
          response.body(body);
          return response.body();
        });
        after((req, res) ->{});
        
        awaitInitialization();
        
        System.out.println("Waiting to handle requests!");
    }

}
