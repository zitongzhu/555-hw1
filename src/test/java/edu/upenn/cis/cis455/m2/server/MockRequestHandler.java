package edu.upenn.cis.cis455.m2.server;

import java.lang.System.Logger.Level;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.RequestEntity;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseEntity;
import edu.upenn.cis.cis455.m2.interfaces.Response;

public class MockRequestHandler implements Route {

    @Override
    public Object handle(Request request, Response response) throws HaltException {
        response.status(200);
        response.type("text/html");

        return "<html><head><title>Response</title></head><body><h1>Response</h1><p>Test</p></body></html>";
    }

}
